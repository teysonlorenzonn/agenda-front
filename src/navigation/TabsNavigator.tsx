/**
 * Learn more about createBottomTabNavigator:
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import * as React from 'react';

import TabBar from './TabBar';
import User from '../screens/user/UserScreen';
import Lists from '../screens/lists/ListsScreen';
import { BottomTabParamList } from './types';

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export const BottomTabNavigator = () => {
  return (
    <BottomTab.Navigator tabBar={(props) => <TabBar {...props} />}>
      <BottomTab.Screen
        name="Lists"
        component={Lists}
        options={{
          title: 'Listas',
        }}
      />
      <BottomTab.Screen
        name="User"
        component={User}
        options={{
          title: 'Usuário',
        }}
      />
    </BottomTab.Navigator>
  );
};
