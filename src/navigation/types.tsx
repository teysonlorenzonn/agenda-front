export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
  Login: undefined;
  Account: undefined;
  ResetPass: undefined;
  ItemList: { idList?: string };
};

export type BottomTabParamList = {
  User: undefined;
  Lists: undefined;
};
