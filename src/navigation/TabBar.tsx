import React, { FC } from 'react';
import { TouchableOpacity, View as ViewRN } from 'react-native';
import { View, Text } from '../components/Themed';
import {
  BottomTabBarProps,
  BottomTabBarOptions,
} from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';
import useTheme from '../hooks/useTheme';

const TabBarIcon = (props: {
  name: React.ComponentProps<typeof Ionicons>['name'];
  color: string;
}) => {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
};

const RederIcon = ({ type, color }: { type: string; color: string }) => {
  switch (type) {
    case 'User':
      return <TabBarIcon name="person-circle-sharp" color={color} />;
    case 'Lists':
      return <TabBarIcon name="list-outline" color={color} />;
    default:
      return null;
  }
};

const TabBar: FC<BottomTabBarProps<BottomTabBarOptions>> = ({
  state,
  descriptors,
  navigation,
}) => {
  const { routes } = state;
  const theme = useTheme();

  return (
    <View
      style={{
        flexDirection: 'row',
        backgroundColor: theme.baseViews,

        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      {routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const colorText = isFocused ? theme.base : theme.tint;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            key={`tab-bar-${index}`}
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{
              flex: 1,
              alignItems: 'center',
              paddingBottom: 5,
              paddingTop: 5,
              borderTopColor: isFocused ? theme.base : theme.tint,
              borderTopWidth: 1,
            }}
          >
            <ViewRN style={{ alignItems: 'center' }}>
              <RederIcon type={route.name} color={colorText} />
              <Text
                style={{
                  color: colorText,
                }}
              >
                {label}
              </Text>
            </ViewRN>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export default TabBar;
