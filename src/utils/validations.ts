import { contains, isEmpty, trim } from 'ramda';

const hasLengthZero = (value: string, msg: string) =>
  value.length === 0 ? '' : msg;

export const hasEmail = (value: string) => {
  if (/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(trim(value))) {
    return { valid: true, msg: '' };
  }
  const final = hasLengthZero(value, 'O email deve conter ex: @dominio.com');
  return { valid: false, msg: final };
};

export const hasUserAndEmail = (value: string) => {
  if (contains('@', value)) {
    return hasEmail(value);
  }

  return haslengthTree(value);
};

export const haslengthTree = (value: string) => {
  if (value.length >= 3) {
    return { valid: true, msg: '' };
  }

  const final = hasLengthZero(value, 'O deve conter pelomenos 3 caracteres');

  return { valid: false, msg: final };
};

export const haslengthSix = (value: string) => {
  if (value.length >= 6) {
    return { valid: true, msg: '' };
  }

  const final = hasLengthZero(value, 'O deve conter pelomenos 6 caracteres');

  return { valid: false, msg: final };
};

export const hasPass = (passOne: string, passTwo: string) => {
  if (passOne.length === 0)
    return { validOne: false, msg: '', validTwo: false };
  if (passOne.length < 6)
    return {
      validOne: false,
      msg: 'A senha deve conter 6 ou mais caracteres',
      validTwo: false,
    };
  if (passOne === passTwo)
    return {
      validOne: true,
      msg: '',
      validTwo: true,
    };

  return {
    validOne: false,
    msg: 'As senhas não conferem',
    validTwo: true,
  };
};

export const hasPhone = (value: string) => {
  value = value.replace(/[^\d]+/g, '');
  if (value.length >= 10) {
    return {
      valid: true,
      msg: '',
    };
  }
  const final = hasLengthZero(
    value,
    'O telefone deve conter 12 ou 13 caracteres',
  );

  return {
    valid: isEmpty(value) ? true : false,
    msg: final,
  };
};
