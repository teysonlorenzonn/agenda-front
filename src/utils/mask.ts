export const maskPhone = (phone: string) => {
  if (phone.replace(/\D/g, '').length <= 10) {
    return phone
      .replace(/\D/g, '')
      .replace(/(\d{2})(\d)/, '($1) $2')
      .replace(/(\d{4})(\d)/, '$1-$2')
      .trim();
  } else {
    return phone
      .replace(/\D/g, '')
      .replace(/(\d{2})(\d)/, '($1) $2')
      .replace(/(\d{5})(\d)/, '$1-$2')
      .trim();
  }
};

export const removeMaskToNumber = (value: string) => {
  return value.replace(/\D/g, '');
};
