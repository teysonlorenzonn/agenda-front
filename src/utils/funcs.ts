import { toLower } from 'ramda';

export const filterLike = (value: string, propRefer: string, data: any) =>
  data.filter((e: any) => toLower(e[propRefer]).includes(toLower(value)));
