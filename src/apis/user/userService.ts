import { LoginProps, UserProps } from '../../store/reducers/user/types';
import request, { Requests } from '../axiosConfig';
import { removeCookie, setCookie } from '../cookies';

export const logInService = async (body: LoginProps) => {
  const resp: Requests = await request
    .post('user/login', body)
    .then(({ data }: { data: Requests }) => {
      if (data['data']) {
        setCookie('token', data['data']['token']);
        setCookie('tokenType', data['data']['type']);
      }

      return data;
    })
    .catch((e: Requests) =>
      typeof e.message === 'string'
        ? e
        : { ...e, message: 'Servidor em manutenção' },
    );

  return resp;
};

export const getUserService = async () => {
  const resp: Requests = await request
    .get('user')
    .then(({ data }: { data: Requests }) => data)
    .catch((e: Requests) =>
      typeof e.message === 'string'
        ? e
        : { ...e, message: 'Servidor em manutenção' },
    );

  return resp;
};

export const logOutService = async () => {
  const resp: Requests = await request
    .post('user/logout')
    .then(({ data }: { data: Requests }) => {
      removeCookie('token');
      removeCookie('tokenType');
      return data;
    })
    .catch((e: Requests) =>
      typeof e.message === 'string'
        ? e
        : { ...e, message: 'Servidor em manutenção' },
    );

  return resp;
};

export const createUserService = async (body: UserProps) => {
  const resp: Requests = await request
    .post('user', body)
    .then(({ data }: { data: Requests }) => data)
    .catch((e: Requests) =>
      typeof e.message === 'string'
        ? e
        : { ...e, message: 'Servidor em manutenção' },
    );

  return resp;
};

export const updateUserService = async (body: UserProps) => {
  const resp: Requests = await request
    .put('user', body)
    .then(({ data }: { data: Requests }) => data)
    .catch((e: Requests) =>
      typeof e.message === 'string'
        ? e
        : { ...e, message: 'Servidor em manutenção' },
    );

  return resp;
};

export const resetPassUserService = async (body: UserProps) => {
  const resp: Requests = await request
    .put('user/password', body)
    .then(({ data }: { data: Requests }) => data)
    .catch((e: Requests) =>
      typeof e.message === 'string'
        ? e
        : { ...e, message: 'Servidor em manutenção' },
    );

  return resp;
};
