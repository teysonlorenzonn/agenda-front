import { ListsProps } from '../../store/reducers/lists/types';
import request, { Requests } from '../axiosConfig';

export const getListsService = async () => {
  const resp: Requests = await request
    .get('list/all')
    .then(({ data }: { data: Requests }) => data)
    .catch((e: Requests) =>
      typeof e.message === 'string'
        ? e
        : { ...e, message: 'Servidor em manutenção' },
    );

  return resp;
};

export const createListsService = async (body: ListsProps) => {
  const resp: Requests = await request
    .post('list', body)
    .then(({ data }: { data: Requests }) => data)
    .catch((e: Requests) =>
      typeof e.message === 'string'
        ? e
        : { ...e, message: 'Servidor em manutenção' },
    );

  return resp;
};

export const updateListsService = async (body: ListsProps) => {
  const resp: Requests = await request
    .put('list', body)
    .then(({ data }: { data: Requests }) => data)
    .catch((e: Requests) =>
      typeof e.message === 'string'
        ? e
        : { ...e, message: 'Servidor em manutenção' },
    );

  return resp;
};

export const deleteListsService = async (id: string) => {
  const resp: Requests = await request
    .delete('list', { data: { id } })
    .then(({ data }: { data: Requests }) => data)
    .catch((e: Requests) =>
      typeof e.message === 'string'
        ? e
        : { ...e, message: 'Servidor em manutenção' },
    );

  return resp;
};
