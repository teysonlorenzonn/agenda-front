import AsyncStorage from '@react-native-async-storage/async-storage';

export const setCookie = async (name: string, value: string) => {
  try {
    await AsyncStorage.setItem(name, value);
  } catch (e) {
    console.log('Erro ao setar cookie', e);
  }
};

export const getCookie = async (name: string) => {
  let cookie = '';
  await AsyncStorage.getItem(name, (error, resp) => {
    if (!error) {
      cookie = resp ? resp : '';
    }
  });
  return cookie;
};

export const removeCookie = async (name: string) => {
  try {
    await AsyncStorage.removeItem(name);
  } catch (e) {
    console.log('Erro ao remover o cookie', e);
  }
};
