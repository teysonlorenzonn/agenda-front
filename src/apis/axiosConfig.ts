import axios from 'axios';
import { isEmpty } from 'ramda';
import { getCookie } from './cookies';

export type Requests = {
  message: any;
  data?: any;
  success: boolean;
};

const request = axios.create({
  baseURL: 'https://agenda-apis.herokuapp.com',
});

request.interceptors.request.use(
  async (config) => {
    const token = await getCookie('token');

    if (!isEmpty(token)) {
      config.headers.Authorization = token;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  },
);

request.interceptors.response.use(
  (response) => {
    return response;
  },
  ({ response }) => {
    return Promise.reject(response.data);
  },
);

export default request;
