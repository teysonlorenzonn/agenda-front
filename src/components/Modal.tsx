import React, { ReactNode, FC } from 'react';
import {
  TouchableWithoutFeedback,
  Modal,
  StyleSheet,
  View,
} from 'react-native';
import { Theme } from '../constants/Colors';
import useTheme from '../hooks/useTheme';

type ModalView = {
  open: boolean;
  setOpen: any;
  children: ReactNode;
  render: ReactNode;
};

const ModalView: FC<ModalView> = ({ open, setOpen, children, render }) => {
  const theme = useTheme();
  const styles = useStyles(theme);

  return (
    <>
      <Modal
        animationType="slide"
        transparent={true}
        visible={open}
        style={{ zIndex: 999 }}
        onRequestClose={() => {
          setOpen(false);
        }}
      >
        <TouchableWithoutFeedback onPress={() => setOpen(false)}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>{render}</View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
      {children}
    </>
  );
};

const useStyles = (theme: Theme) =>
  StyleSheet.create({
    centeredView: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    modalView: {
      margin: 20,
      backgroundColor: theme.baseViews,
      borderRadius: 8,
      padding: 35,
      alignItems: 'center',
      shadowColor: 'black',
      shadowOffset: {
        width: 0,
        height: 0,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
    },
  });

ModalView.defaultProps = {
  open: false,
};

export default ModalView;
