import React, { FC, ReactNode } from 'react';
import { View } from 'react-native';
import Loader, { LoaderProps } from './Loader';

const LoaderPage: FC<LoaderProps & { children: ReactNode; active?: boolean }> =
  ({ children, active, ...props }) => (
    <>
      {active && (
        <>
          <View
            style={{
              position: 'absolute',
              backgroundColor: '#525252',
              opacity: 0.6,
              width: '100%',
              height: 4000,
              zIndex: 9998,
            }}
          />
          <Loader {...props} />
        </>
      )}
      {children}
    </>
  );

LoaderPage.defaultProps = { active: false, typeLoad: 'Fold', size: 45 };

export default LoaderPage;
