import React, { ReactChild } from 'react';
import { KeyboardAvoidingView, Platform } from 'react-native';

export default ({ children }: { children: ReactChild }) => (
  <KeyboardAvoidingView
    style={{ width: '100%', height: '100%' }}
    behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
  >
    {children}
  </KeyboardAvoidingView>
);
