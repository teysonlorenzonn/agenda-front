import React, { useState, FC } from 'react';
import { Ionicons as Icon } from '@expo/vector-icons';
import { SpeedDial } from 'react-native-elements';
import { StyleSheet } from 'react-native';
import { StackScreenProps } from '@react-navigation/stack';

import useTheme from '../hooks/useTheme';
import { Theme } from '../constants/Colors';
import { ReducersState, withRedux } from '../store/connectRedux';
import { logOut } from '../store/reducers/user/userReducer';
import { RootStackParamList } from '../navigation/types';
import LoaderPage from './LoaderContent';
import { useSelector } from 'react-redux';

type SpeedProps = {
  add?: any;
  search?: any;
  logOut?: any;
  functionAdd?: any;
  functionSearch?: any;
} & StackScreenProps<RootStackParamList, 'Login'>;

const Speed: FC<SpeedProps> = ({
  add,
  search,
  functionSearch,
  functionAdd,
  logOut,
  navigation,
}) => {
  const theme = useTheme();
  const styles = useStyles(theme);
  const [open, setOpen] = useState(false);
  const { loading } = useSelector((state: ReducersState) => state.user);

  const resetNavigation = () =>
    navigation.reset({
      index: 0,
      routes: [{ name: 'Login' }],
    });

  return (
    <LoaderPage active={loading} color={theme.base}>
      <SpeedDial
        isOpen={open}
        icon={<Icon name="options" size={25} />}
        color={theme.base}
        openIcon={<Icon name="close" size={25} />}
        onOpen={() => setOpen(!open)}
        onClose={() => setOpen(!open)}
      >
        {add && (
          <SpeedDial.Action
            icon={<Icon name="add" size={22} style={{ paddingLeft: 2 }} />}
            color={theme.base}
            title="Nova lista"
            titleStyle={styles.titleSpeed}
            onPress={() => {
              functionAdd();
              setOpen(false);
            }}
          />
        )}

        {search && (
          <SpeedDial.Action
            icon={<Icon name="search" size={19} style={{ paddingLeft: 2 }} />}
            color={theme.base}
            title="Buscar"
            titleStyle={styles.titleSpeed}
            onPress={() => {
              functionSearch();
              setOpen(false);
            }}
          />
        )}

        <SpeedDial.Action
          icon={
            <Icon name="log-out-outline" size={22} style={{ paddingLeft: 2 }} />
          }
          color={theme.base}
          titleStyle={styles.titleSpeed}
          title="Sair"
          onPress={() => {
            logOut(resetNavigation);
            setOpen(false);
          }}
        />
      </SpeedDial>
    </LoaderPage>
  );
};

Speed.defaultProps = {
  add: false,
  search: false,
};

const useStyles = (theme: Theme) =>
  StyleSheet.create({
    titleSpeed: { backgroundColor: 'transparent', color: theme.text },
  });

export default withRedux(null, { logOut })(Speed);
