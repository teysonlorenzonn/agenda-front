import { isEmpty } from 'ramda';
import React, { FC } from 'react';
import {
  Input as InputRE,
  InputProps as InputPropsRE,
} from 'react-native-elements';

import useTheme from '../hooks/useTheme';

type InputProps = {
  validation?: any;
  nameInput?:
    | 'password'
    | 'login'
    | 'firstName'
    | 'lastName'
    | 'email'
    | 'phoneNumber'
    | 'userName'
    | 'confirmPassword'
    | 'currentPassword';
};

const Input: FC<InputProps & InputPropsRE> = ({
  validation,
  nameInput,
  inputStyle,
  labelStyle,
  ...props
}) => {
  const theme = useTheme();

  return (
    <InputRE
      {...props}
      inputStyle={[{ color: theme.text }, inputStyle]}
      labelStyle={[{ color: theme.text }, labelStyle]}
      inputContainerStyle={{
        borderBottomColor:
          !validation || !nameInput || isEmpty(validation[nameInput]['msg'])
            ? theme.text
            : 'red',
      }}
      errorMessage={validation && nameInput ? validation[nameInput]['msg'] : ''}
    />
  );
};

export default Input;
