import React, { FC } from 'react';
import { Button as ButtonRE, ButtonProps } from 'react-native-elements';
import useTheme from '../hooks/useTheme';

const Button: FC<ButtonProps & { width?: number }> = ({
  disabledStyle,
  disabledTitleStyle,
  titleStyle,
  buttonStyle,
  width,
  ...props
}) => {
  const theme = useTheme();

  return (
    <ButtonRE
      {...props}
      disabledStyle={[{ borderColor: theme.disabled }, disabledStyle]}
      disabledTitleStyle={[{ color: theme.disabled }, disabledTitleStyle]}
      titleStyle={[
        {
          color: theme.base,
        },
        titleStyle,
      ]}
      buttonStyle={[
        {
          borderColor: theme.base,
          borderRadius: 8,
          width: width,
        },
        buttonStyle,
      ]}
    />
  );
};

Button.defaultProps = {
  width: 250,
};
export default Button;
