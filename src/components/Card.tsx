import React, { FC } from 'react';
import { Card as CardRE, CardProps } from 'react-native-elements';
import useTheme from '../hooks/useTheme';

const Card: FC<CardProps> = ({ containerStyle, ...props }) => {
  const theme = useTheme();

  return (
    <CardRE
      {...props}
      containerStyle={[
        {
          backgroundColor: theme.baseViews,
          borderWidth: 0,
          borderRadius: 8,
          marginBottom: 20,
        },
        containerStyle,
      ]}
    />
  );
};

export default Card;
