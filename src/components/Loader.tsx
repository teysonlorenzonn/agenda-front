import React, { FC } from 'react';
import { View, StyleProp, ViewStyle } from 'react-native';
import * as Spin from 'react-native-animated-spinkit';

export type LoaderProps = {
  size?: number;
  color: string;
  style?: StyleProp<ViewStyle>;
  typeLoad?:
    | 'Chase'
    | 'Bounce'
    | 'Circle'
    | 'CircleFade'
    | 'Flow'
    | 'Fold'
    | 'Grid'
    | 'Plane'
    | 'Pulse'
    | 'Swing'
    | 'Wander'
    | 'Wave';
};

const Loader: FC<LoaderProps> = ({ size, color, style, typeLoad }) => {
  const Component = Spin[!typeLoad ? 'Fold' : typeLoad];

  return (
    <View
      style={[
        style,
        {
          zIndex: 9999,
          display: 'flex',
          position: 'absolute',
          top: '50%',
          left: '45%',
        },
      ]}
    >
      <Component size={size} color={color} />
    </View>
  );
};

export default Loader;
