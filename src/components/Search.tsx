import React, { useRef, FC } from 'react';
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons';
import {
  Animated,
  Keyboard,
  TextInput,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import { Theme } from '../constants/Colors';
import useTheme from '../hooks/useTheme';

type SearchProps = {
  setOpenSearch: any;
  openSearch: boolean;
  search: string;
  setSearch: any;
  children: any;
};

const Search: FC<SearchProps> = ({
  setOpenSearch,
  openSearch,
  search,
  setSearch,
  children,
}) => {
  const fadeAnim = useRef(new Animated.Value(0)).current;
  const searchRef = useRef<TextInput>(null);
  const theme = useTheme();
  const styles = useStyles(theme, openSearch);

  const fadeIn = () => {
    setOpenSearch(true);
    Animated.timing(fadeAnim, {
      useNativeDriver: true,
      toValue: 1,
      duration: 300,
    }).start(({ finished }) => finished && searchRef.current?.focus());
  };

  const fadeOut = () => {
    Animated.timing(fadeAnim, {
      useNativeDriver: true,
      toValue: 0,
      duration: 300,
    }).start(({ finished }) => {
      if (finished) {
        Keyboard.dismiss();
        setOpenSearch(false);
        setSearch('');
      }
    });
  };

  return (
    <>
      <Animated.View style={[styles.searchView, { opacity: fadeAnim }]}>
        <TextInput
          ref={searchRef}
          style={styles.search}
          placeholder="Buscar ..."
          value={search}
          onChangeText={setSearch}
          onBlur={() => search.length === 0 && fadeOut()}
        />
        <TouchableOpacity style={styles.closeIcon} onPress={fadeOut}>
          <Icon name="close" size={17} color={theme.tint} />
        </TouchableOpacity>
      </Animated.View>
      {children({ fadeIn })}
    </>
  );
};

const useStyles = (theme: Theme, openSearch?: boolean) =>
  StyleSheet.create({
    search: {
      color: theme.base,
      paddingTop: 10,
      paddingBottom: 10,
      paddingLeft: 10,
      fontSize: 20,
      width: '92%',
    },
    searchView: {
      backgroundColor: theme.baseViews,
      display: openSearch ? 'flex' : 'none',
      margin: 10,
      borderRadius: 8,
      flexDirection: 'row',
      justifyContent: 'space-between',
      height: 50,
    },
    closeIcon: {
      color: theme.tint,
      fontSize: 17,
      paddingRight: 2,
      width: '8%',
      height: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
  });

export default Search;
