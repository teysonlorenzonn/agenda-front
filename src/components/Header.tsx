import React, { FC } from 'react';
import { Ionicons as Icon } from '@expo/vector-icons';
import { TouchableOpacity, StyleSheet } from 'react-native';
import { Header as HeaderRE, HeaderProps } from 'react-native-elements';

import useTheme from '../hooks/useTheme';
import { Theme } from '../constants/Colors';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from '../navigation/types';

type HeaderProp = {
  functionPressBack?: any;
  title: string;
} & StackScreenProps<
  RootStackParamList,
  'Account' | 'ItemList' | 'Login' | 'NotFound' | 'ResetPass' | 'Root'
>;

const Header: FC<HeaderProps & HeaderProp> = ({
  functionPressBack,
  navigation,
  title,
  ...props
}) => {
  const theme = useTheme();
  const styles = useStyles(theme);

  return (
    <HeaderRE
      {...props}
      leftComponent={
        <TouchableOpacity
          onPress={() => {
            functionPressBack && functionPressBack();
            navigation.goBack();
          }}
        >
          <Icon name="chevron-back" color={theme.text} size={23} />
        </TouchableOpacity>
      }
      backgroundColor={theme.baseViews}
      containerStyle={styles.header}
      centerComponent={{
        text: title,
        style: styles.centerContent,
      }}
    />
  );
};

const useStyles = (theme: Theme) =>
  StyleSheet.create({
    header: {
      borderBottomColor: theme.base,
    },
    centerContent: {
      paddingTop: 1,
      fontSize: 20,
      color: theme.tint,
    },
  });

export default Header;
