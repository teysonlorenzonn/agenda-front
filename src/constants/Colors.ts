const tintColorLight = '#000';
const tintColorDark = '#fff';

export type Theme = {
  text: string;
  background: string;
  tint: string;
  base: string;
  baseViews: string;
  tabIconDefault: string;
  tabIconSelected: string;
  disabled: string;
};

export type TypeTheme = {
  light: Theme;
  dark: Theme;
};

export default {
  light: {
    text: '#000',
    background: '#fff',
    tint: tintColorLight,
    base: '#7957f2',
    baseViews: '#fff',
    baseTabs: '#7957f2',
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight,
    disabled: '#8d8d8d',
  },
  dark: {
    text: '#fff',
    background: '#000',
    tint: tintColorDark,
    base: '#ffb000',
    baseViews: '#272727',
    baseTabs: '#272727',
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark,
    disabled: '#8d8d8d',
  },
};
