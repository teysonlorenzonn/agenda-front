import Colors from '../constants/Colors';
import useColorScheme from './useColorScheme';

export default () => {
  const colorScheme = useColorScheme();
  const theme = Colors[colorScheme];

  return theme;
};
