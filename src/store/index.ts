import { createStore, applyMiddleware } from 'redux';
import { Reducers } from './reducers/connectorReducers';

import createSagaMiddleware from 'redux-saga';

export const configureStore = () => {
  const sagaMiddleware = createSagaMiddleware();
  const create = createStore(Reducers, applyMiddleware(sagaMiddleware));

  return {
    runSaga: sagaMiddleware.run,
    ...create,
  };
};
