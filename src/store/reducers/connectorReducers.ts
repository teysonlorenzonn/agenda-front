import { combineReducers } from 'redux';
import { listsReducer } from './lists/listsReducer';
import { userReducer } from './user/userReducer';

export const Reducers = combineReducers({
  user: userReducer,
  lists: listsReducer,
});
