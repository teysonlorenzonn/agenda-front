import { all, fork } from 'redux-saga/effects';
import { listsSaga } from './lists/listsReducer';
import { userSaga } from './user/userReducer';

export function* sagas() {
  yield all([fork(userSaga), fork(listsSaga)]);
}
