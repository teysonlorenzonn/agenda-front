export type UserProps = {
  id?: string;
  firstName: string;
  lastName: string;
  email: string;
  userName: string;
  password: string;
  phoneNumber?: string;
};

export type StateUserProps = {
  loading: boolean;
  user: UserProps;
  response: {
    message: string;
    success: boolean;
  };
};

export type ActionUser = {
  payload: UserProps;
};

export type LoginProps = {
  login: string;
  password: string;
};
