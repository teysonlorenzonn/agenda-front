import { takeLatest, put } from 'redux-saga/effects';
import { createReducer } from 'redux-create-reducer';
import { createAction } from 'redux-actions';
import { LoginProps, StateUserProps, UserProps } from './types';
import {
  logInService,
  getUserService,
  logOutService,
  createUserService,
  updateUserService,
  resetPassUserService,
} from '../../../apis/user/userService';
import { Requests } from '../../../apis/axiosConfig';
import { getCookie } from '../../../apis/cookies';
import { isEmpty } from 'ramda';

const LOADING_USER = 'LOADING_USER';
const LOADING_USER_FINISH = 'LOADING_USER_FINISH';
const RESET_RESPONSE_USER = 'RESET_RESPONSE_USER';
const ERROR_USER = 'ERROR_USER';
const SET_USER = 'SET_USER';
const LOGOUT = 'LOGOUT';
const LOGIN = 'LOGIN';
const CREATE_USER = 'CREATE_USER';
const UPDATE_USER = 'UPDATE_USER';
const RESET_PASS_USER = 'RESET_PASS_USER';
const FETCH_USER = 'FETCH_USER';
const RESET_USER = 'RESET_USER';

export const logIn = createAction(LOGIN);
export const logOut = createAction(LOGOUT);
export const fetchUser = createAction(FETCH_USER);
export const updateAccount = createAction(UPDATE_USER);
export const updatePassAccount = createAction(RESET_PASS_USER);
export const createAccount = createAction(CREATE_USER);
export const resetResponseUser = createAction(RESET_RESPONSE_USER);

const initialState: StateUserProps = {
  loading: false,
  user: {
    firstName: '',
    lastName: '',
    email: '',
    userName: '',
    password: '',
    phoneNumber: '',
  },
  response: {
    message: '',
    success: false,
  },
};

export const userReducer = createReducer(initialState, {
  [SET_USER]: (state, { payload }) => ({
    ...state,
    user: {
      ...payload.user,
    },
    response: {
      ...payload.response,
    },
  }),

  [LOADING_USER]: (state) => ({
    ...state,
    loading: true,
  }),

  [LOADING_USER_FINISH]: (state) => ({
    ...state,
    loading: false,
  }),

  [ERROR_USER]: (state, { payload }) => ({
    ...state,
    response: {
      message: payload,
      success: false,
    },
  }),

  [RESET_RESPONSE_USER]: (state) => ({
    ...state,
    response: {
      message: '',
      success: false,
    },
  }),

  [RESET_USER]: () => initialState,
});

function* getLogin({ payload }: { payload: LoginProps }) {
  try {
    yield put({ type: LOADING_USER });
    const login: Requests = yield logInService(payload);
    if (login.success) {
      const user: Requests = yield getUserService();
      yield put({
        type: SET_USER,
        payload: {
          user: user.data,
          response: {
            message: user.message,
            success: user.success,
          },
        },
      });
    } else {
      yield put({
        type: ERROR_USER,
        payload: login.message,
      });
    }
  } catch (e) {
    yield put({
      type: ERROR_USER,
      payload: e,
    });
  } finally {
    yield put({ type: LOADING_USER_FINISH });
  }
}

function* getProfile() {
  try {
    const token: string = yield getCookie('token');

    if (!isEmpty(token)) {
      yield put({ type: LOADING_USER });
      const user: Requests = yield getUserService();
      if (user.success) {
        yield put({
          type: SET_USER,
          payload: {
            user: user.data,
            response: {
              message: user.message,
              success: user.success,
            },
          },
        });
      } else {
        yield put({
          type: ERROR_USER,
          payload: user.message,
        });
      }
    }
  } catch (e) {
    yield put({
      type: ERROR_USER,
      payload: e,
    });
  } finally {
    yield put({ type: LOADING_USER_FINISH });
  }
}

function* getLogout({ payload }: { payload: any }) {
  try {
    yield put({ type: LOADING_USER });
    const login: Requests = yield logOutService();
    if (login.success) {
      yield put({
        type: RESET_USER,
      });
      payload();
    } else {
      yield put({
        type: ERROR_USER,
        payload: login.message,
      });
    }
  } catch (e) {
    yield put({
      type: ERROR_USER,
      payload: e,
    });
  } finally {
    yield put({ type: LOADING_USER_FINISH });
  }
}

function* createUser({ payload }: { payload: UserProps }) {
  try {
    yield put({ type: LOADING_USER });
    const create: Requests = yield createUserService(payload);
    if (create.success) {
      yield put({
        type: LOGIN,
        payload: { login: payload.userName, password: payload.password },
      });
    } else {
      yield put({
        type: ERROR_USER,
        payload: create.message,
      });
      yield put({ type: LOADING_USER_FINISH });
    }
  } catch (e) {
    yield put({
      type: ERROR_USER,
      payload: e,
    });
    yield put({ type: LOADING_USER_FINISH });
  }
}

function* updateUser({ payload }: { payload: UserProps }) {
  try {
    yield put({ type: LOADING_USER });
    const update: Requests = yield updateUserService(payload);
    if (update.success) {
      yield put({
        type: FETCH_USER,
      });
    } else {
      yield put({
        type: ERROR_USER,
        payload: update.message,
      });
      yield put({ type: LOADING_USER_FINISH });
    }
  } catch (e) {
    yield put({
      type: ERROR_USER,
      payload: e,
    });
    yield put({ type: LOADING_USER_FINISH });
  }
}

function* resetPassUser({ payload }: { payload: UserProps }) {
  try {
    yield put({ type: LOADING_USER });
    const update: Requests = yield resetPassUserService(payload);
    if (!update.success) {
      yield put({
        type: ERROR_USER,
        payload: update.message,
      });
    }
  } catch (e) {
    yield put({
      type: ERROR_USER,
      payload: e,
    });
  } finally {
    yield put({ type: LOADING_USER_FINISH });
  }
}

export function* userSaga() {
  yield takeLatest<any>(LOGIN, getLogin);
  yield takeLatest<any>(LOGOUT, getLogout);
  yield takeLatest<any>(CREATE_USER, createUser);
  yield takeLatest<any>(FETCH_USER, getProfile);
  yield takeLatest<any>(UPDATE_USER, updateUser);
  yield takeLatest<any>(RESET_PASS_USER, resetPassUser);
}
