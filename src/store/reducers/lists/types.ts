export type ListsProps = {
  id?: string;
  name: string;
  description: string;
  items: Array<ItemsListProps | []>;
};

export type ItemsListProps = {
  id?: string;
  name: string;
  quantity: number;
  price?: string;
};

export type StateListsProps = {
  loading: boolean;
  lists: Array<ListsProps>;
  response: {
    message: string;
    success: boolean;
  };
};

export type ActionLists = {
  payload: ListsProps;
};
