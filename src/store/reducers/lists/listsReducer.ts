import { takeLatest, put, select } from 'redux-saga/effects';
import { createReducer } from 'redux-create-reducer';
import { createAction } from 'redux-actions';
import { StateListsProps, ActionLists, ListsProps } from './types';
import {
  getListsService,
  createListsService,
  deleteListsService,
  updateListsService,
} from '../../../apis/lists/listsService';
import { Requests } from '../../../apis/axiosConfig';
import { union } from 'ramda';
import { ReducersState } from '../../connectRedux';

const LOADING_LISTS = 'LOADING_LISTS';
const LOADING_LISTS_FINISH = 'LOADING_LISTS_FINISH';

const ERROR_LISTS = 'ERROR_LISTS';
const SET_LISTS = 'SET_LISTS';
const CREATE_LISTS = 'CREATE_LISTS';
const DELETE_LISTS = 'DELETE_LISTS';
const UPDATE_LISTS = 'UPDATE_LISTS';
const FETCH_LISTS = 'FETCH_LISTS';
const RESET_LISTS = 'RESET_LISTS';
const RESET_RESPONSE_LISTS = 'RESET_RESPONSE_LISTS';

export const fetchLists = createAction(FETCH_LISTS);
export const createList = createAction(CREATE_LISTS);
export const updateList = createAction(UPDATE_LISTS);
export const deleteList = createAction(DELETE_LISTS);
export const resetResponseLists = createAction(RESET_RESPONSE_LISTS);

const initialState: StateListsProps = {
  loading: false,
  lists: [],
  response: {
    message: '',
    success: false,
  },
};

export const listsReducer = createReducer(initialState, {
  [SET_LISTS]: (state, { payload }) => ({
    ...state,
    lists: payload.lists,
    response: {
      ...payload.response,
    },
  }),

  [LOADING_LISTS]: (state) => ({
    ...state,
    loading: true,
  }),

  [LOADING_LISTS_FINISH]: (state) => ({
    ...state,
    loading: false,
  }),

  [ERROR_LISTS]: (state, { payload }) => ({
    ...state,
    response: {
      message: payload,
      success: false,
    },
  }),

  [RESET_RESPONSE_LISTS]: (state) => ({
    ...state,
    response: {
      message: '',
      success: false,
    },
  }),

  [RESET_LISTS]: () => initialState,
});

function* getLists() {
  try {
    yield put({ type: LOADING_LISTS });
    const lists: Requests = yield getListsService();
    if (lists.success) {
      yield put({
        type: SET_LISTS,
        payload: {
          lists: lists.data,
          response: {
            message: lists.message,
            success: lists.success,
          },
        },
      });
    } else {
      yield put({
        type: ERROR_LISTS,
        payload: lists.message,
      });
    }
  } catch (e) {
    yield put({
      type: ERROR_LISTS,
      payload: e,
    });
  } finally {
    yield put({ type: LOADING_LISTS_FINISH });
  }
}

function* createLists({ payload }: ActionLists) {
  try {
    yield put({ type: LOADING_LISTS });
    const lists: Array<ListsProps> = yield select(
      (state: ReducersState) => state.lists.lists,
    );

    const create: Requests = yield createListsService(payload);
    if (create.success) {
      const data = union([create.data], lists);
      yield put({
        type: SET_LISTS,
        payload: {
          lists: data,
          response: {
            message: create.message,
            success: create.success,
          },
        },
      });
    } else {
      yield put({
        type: ERROR_LISTS,
        payload: create.message,
      });
    }
  } catch (e) {
    yield put({
      type: ERROR_LISTS,
      payload: e,
    });
  } finally {
    yield put({ type: LOADING_LISTS_FINISH });
  }
}

function* updateLists({ payload }: ActionLists) {
  try {
    yield put({ type: LOADING_LISTS });
    const lists: Array<ListsProps> = yield select(
      (state: ReducersState) => state.lists.lists,
    );

    const update: Requests = yield updateListsService(payload);
    if (update.success) {
      const data = union([update.data], lists);
      yield put({
        type: SET_LISTS,
        payload: {
          lists: data,
          response: {
            message: update.message,
            success: update.success,
          },
        },
      });
    } else {
      yield put({
        type: ERROR_LISTS,
        payload: update.message,
      });
    }
  } catch (e) {
    yield put({
      type: ERROR_LISTS,
      payload: e,
    });
  } finally {
    yield put({ type: LOADING_LISTS_FINISH });
  }
}

function* deleteLists({ payload }: { payload: string }) {
  try {
    yield put({ type: LOADING_LISTS });
    const delet: Requests = yield deleteListsService(payload);
    if (delet.success) {
      yield put({
        type: FETCH_LISTS,
      });
    } else {
      yield put({
        type: ERROR_LISTS,
        payload: delet.message,
      });
      yield put({ type: LOADING_LISTS_FINISH });
    }
  } catch (e) {
    yield put({
      type: ERROR_LISTS,
      payload: e,
    });
    yield put({ type: LOADING_LISTS_FINISH });
  }
}

export function* listsSaga() {
  yield takeLatest<any>(FETCH_LISTS, getLists);
  yield takeLatest<any>(CREATE_LISTS, createLists);
  yield takeLatest<any>(DELETE_LISTS, deleteLists);
  yield takeLatest<any>(UPDATE_LISTS, updateLists);
}
