import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { StateUserProps } from './reducers/user/types';
import { StateListsProps } from './reducers/lists/types';

export type ReducersState = {
  user: StateUserProps;
  lists: StateListsProps;
};

export const withRedux = (states: any, actions: any) => {
  const mapStateToProps = states ? states : null;

  const mapDispatchToProps = actions
    ? (dispatch: any) => bindActionCreators(actions, dispatch)
    : null;

  return connect(mapStateToProps, mapDispatchToProps);
};
