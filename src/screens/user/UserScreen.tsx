import React, { FC } from 'react';
import { StyleSheet } from 'react-native';
import SpeedDial from '../../components/SpeedDial';

import { View } from '../../components/Themed';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from '../../navigation/types';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AccountForm from './AccountForm';
import useTheme from '../../hooks/useTheme';

import { useSelector } from 'react-redux';
import { ReducersState } from '../../store/connectRedux';
import Card from '../../components/Card';

const User: FC<StackScreenProps<RootStackParamList, 'Login'>> = ({
  navigation,
  route,
}) => {
  const theme = useTheme();
  const styles = useStyles();
  const { user } = useSelector((state: ReducersState) => state.user);

  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView style={{ width: '100%' }}>
        <Card>
          <AccountForm
            navigation={navigation}
            route={route}
            theme={theme}
            user={user}
          />
        </Card>
      </KeyboardAwareScrollView>
      <SpeedDial navigation={navigation} route={route} />
    </View>
  );
};

const useStyles = () =>
  StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 20,
    },
  });

export default User;
