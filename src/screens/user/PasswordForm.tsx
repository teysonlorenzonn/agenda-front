import { isEmpty } from 'ramda';
import React, { FC, useState } from 'react';
import { TouchableOpacity } from 'react-native';
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons';

import Input from '../../components/Input';
import { Theme } from '../../constants/Colors';
import { hasPass } from '../../utils/validations';

type PassProps = {
  form: { password: string; confirmPassword: string };
  validation: {
    password: {
      valid: boolean;
      msg: string;
    };
    confirmPassword: {
      valid: boolean;
      msg: string;
    };
  };
  onSubmit: any;
  setForm: any;
  setValidation: any;
  loading?: boolean;
  theme: Theme;
};

const Password: FC<PassProps> = ({
  form,
  validation,
  onSubmit,
  loading,
  setForm,
  setValidation,
  theme,
}) => {
  const [securityPass, setSecurityPass] = useState(true);
  const [securityCPass, setSecurityCPass] = useState(true);

  return (
    <>
      <Input
        label="Senha"
        placeholder="Digite a senha ..."
        value={form.password}
        nameInput="password"
        validation={validation}
        secureTextEntry={securityPass}
        onSubmitEditing={onSubmit}
        disabled={loading}
        autoCorrect={false}
        keyboardType="visible-password"
        leftIcon={<Icon size={23} name="lock" color={theme.base} />}
        rightIcon={
          <TouchableOpacity onPress={() => setSecurityPass(!securityPass)}>
            <Icon
              size={23}
              name={securityPass ? 'eye-off' : 'eye'}
              color={theme.base}
            />
          </TouchableOpacity>
        }
        onChangeText={(text) => {
          const validate = hasPass(text, form.confirmPassword);
          setForm({
            ...form,
            password: text,
          });
          setValidation({
            ...validation,
            password: {
              valid: validate.validOne,
              msg: validate.msg,
            },
            confirmPassword: {
              msg:
                validate.validTwo || isEmpty(form.confirmPassword)
                  ? ''
                  : validation.password.msg,
              valid: validate.validTwo,
            },
          });
        }}
      />
      <Input
        label="Confirmação de senha"
        placeholder="Digite novamente a senha ..."
        value={form.confirmPassword}
        nameInput="confirmPassword"
        validation={validation}
        secureTextEntry={securityCPass}
        onSubmitEditing={onSubmit}
        disabled={loading}
        keyboardType="visible-password"
        autoCorrect={false}
        leftIcon={<Icon size={23} name="lock" color={theme.base} />}
        rightIcon={
          <TouchableOpacity onPress={() => setSecurityCPass(!securityCPass)}>
            <Icon
              size={23}
              name={securityCPass ? 'eye-off' : 'eye'}
              color={theme.base}
            />
          </TouchableOpacity>
        }
        onChangeText={(text) => {
          const validate = hasPass(text, form.password);
          setForm({
            ...form,
            confirmPassword: text,
          });
          setValidation({
            ...validation,
            confirmPassword: {
              valid: validate.validOne,
              msg: validate.msg,
            },
            password: {
              msg:
                validate.validTwo || isEmpty(form.password)
                  ? ''
                  : validation.password.msg,
              valid: validate.validTwo,
            },
          });
        }}
      />
    </>
  );
};

export default Password;
