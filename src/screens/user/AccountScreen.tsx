import React, { FC } from 'react';
import { StackScreenProps } from '@react-navigation/stack';
import { StyleSheet } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { View } from '../../components/Themed';
import { RootStackParamList } from '../../navigation/types';
import useTheme from '../../hooks/useTheme';
import { Theme } from '../../constants/Colors';
import AccountForm from './AccountForm';
import LoaderPage from '../../components/LoaderContent';
import { useSelector } from 'react-redux';
import { ReducersState } from '../../store/connectRedux';
import Card from '../../components/Card';
import Header from '../../components/Header';

const Account: FC<StackScreenProps<RootStackParamList, 'Account'>> = ({
  navigation,
  route,
}) => {
  const { loading } = useSelector((state: ReducersState) => state.user);

  const theme = useTheme();
  const style = useStyle(theme);

  return (
    <LoaderPage active={loading} color={theme.base}>
      <View style={style.container}>
        <Header title="Criar conta" navigation={navigation} route={route} />
        <KeyboardAwareScrollView>
          <Card>
            <AccountForm navigation={navigation} route={route} theme={theme} />
          </Card>
        </KeyboardAwareScrollView>
      </View>
    </LoaderPage>
  );
};

const useStyle = (theme: Theme) =>
  StyleSheet.create({
    container: {
      flex: 1,
      width: '100%',
    },
    header: {
      borderBottomColor: theme.base,
    },
    centerContent: {
      paddingTop: 1,
      fontSize: 20,
      color: theme.tint,
    },
  });

export default Account;
