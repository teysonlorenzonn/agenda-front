import React, { FC, useState, useEffect } from 'react';
import { StackScreenProps } from '@react-navigation/stack';
import { TouchableOpacity, StyleSheet, View as ViewRN } from 'react-native';
import { MaterialCommunityIcons as IconM } from '@expo/vector-icons';

import { Text, View } from '../../components/Themed';
import { RootStackParamList } from '../../navigation/types';
import Password from './PasswordForm';
import useTheme from '../../hooks/useTheme';
import { ReducersState, withRedux } from '../../store/connectRedux';
import Card from '../../components/Card';
import { Theme } from '../../constants/Colors';
import Input from '../../components/Input';
import { haslengthSix } from '../../utils/validations';
import Button from '../../components/Button';
import {
  resetResponseUser,
  updatePassAccount,
} from '../../store/reducers/user/userReducer';
import LoaderPage from '../../components/LoaderContent';
import Header from '../../components/Header';

type ResetPassProps = {
  loading?: boolean;
  updatePassAccount: any;
  resetResponseUser: any;
  response?: {
    message: string;
    success: boolean;
  };
} & StackScreenProps<RootStackParamList, 'ResetPass'>;

const ResetPass: FC<ResetPassProps> = ({
  loading,
  navigation,
  route,
  updatePassAccount,
  resetResponseUser,
  response,
}) => {
  const theme = useTheme();
  const styles = useStyles(theme);

  const [security, setSecurity] = useState(true);
  const [submit, setSubmit] = useState(true);
  const [error, setError] = useState('');

  useEffect(() => {
    if (!loading && response?.success) {
      resetResponseUser();
      navigation.goBack();
    } else if (response && !response.success) {
      setError(response.message);
    }
  }, [response, loading]);

  const [form, setForm] = useState({
    password: '',
    confirmPassword: '',
    currentPassword: '',
  });

  const [validation, setValidation] = useState({
    password: {
      valid: false,
      msg: '',
    },
    confirmPassword: {
      valid: false,
      msg: '',
    },
    currentPassword: {
      valid: false,
      msg: '',
    },
  });

  useEffect(() => {
    if (
      validation.confirmPassword.valid &&
      validation.currentPassword.valid &&
      validation.password.valid
    ) {
      setSubmit(true);
    } else {
      setSubmit(false);
    }
  }, [validation]);

  const onSubmit = () => {
    updatePassAccount(form);
  };

  return (
    <LoaderPage active={loading} color={theme.base}>
      <View>
        <Header
          functionPressBack={resetResponseUser}
          route={route}
          navigation={navigation}
          title="Trocar senha"
        />
        <Card>
          <Text style={styles.error}>{error}</Text>
          <Input
            label="Senha atual"
            placeholder="Digite a senha atual ..."
            value={form.currentPassword}
            nameInput="currentPassword"
            validation={validation}
            secureTextEntry={security}
            onSubmitEditing={onSubmit}
            disabled={loading}
            keyboardType="visible-password"
            autoCorrect={false}
            leftIcon={<IconM size={23} name="lock" color={theme.base} />}
            rightIcon={
              <TouchableOpacity onPress={() => setSecurity(!security)}>
                <IconM
                  size={23}
                  name={security ? 'eye-off' : 'eye'}
                  color={theme.base}
                />
              </TouchableOpacity>
            }
            onChangeText={(text) => {
              const validate = haslengthSix(text);
              setForm({
                ...form,
                currentPassword: text,
              });
              setValidation({
                ...validation,
                currentPassword: {
                  valid: validate.valid,
                  msg: validate.msg,
                },
              });
            }}
          />
          <Password
            theme={theme}
            loading={loading}
            form={form}
            validation={validation}
            setValidation={setValidation}
            setForm={setForm}
            onSubmit={onSubmit}
          />
          <ViewRN style={styles.buttonContainer}>
            <Button
              title="Confirmar"
              type="outline"
              disabled={!submit || loading}
              onPress={onSubmit}
            />
          </ViewRN>
        </Card>
      </View>
    </LoaderPage>
  );
};

const useStyles = (theme: Theme) =>
  StyleSheet.create({
    container: {
      flex: 1,
      width: '100%',
    },
    header: {
      borderBottomColor: theme.base,
    },
    error: {
      textAlign: 'center',
      color: 'red',
      fontSize: 17,
      marginBottom: 10,
    },
    buttonContainer: {
      alignItems: 'center',
      marginTop: 10,
      marginBottom: 10,
    },
    centerContent: {
      paddingTop: 1,
      fontSize: 20,
      color: theme.tint,
    },
  });

const mapToProps = (state: ReducersState) => {
  return {
    loading: state.user.loading,
    response: state.user.response,
  };
};

export default withRedux(mapToProps, { updatePassAccount, resetResponseUser })(
  ResetPass,
);
