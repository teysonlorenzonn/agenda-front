import React, { useState, useEffect, FC } from 'react';
import { omit, trim } from 'ramda';
import { View, StyleSheet } from 'react-native';
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons';

import Input from '../../components/Input';
import Button from '../../components/Button';
import { Theme } from '../../constants/Colors';
import { hasEmail, haslengthTree, hasPhone } from '../../utils/validations';
import { maskPhone, removeMaskToNumber } from '../../utils/mask';
import { ReducersState, withRedux } from '../../store/connectRedux';
import {
  createAccount,
  resetResponseUser,
  updateAccount,
} from '../../store/reducers/user/userReducer';
import { UserProps } from '../../store/reducers/user/types';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from '../../navigation/types';
import { Text } from '../../components/Themed';
import Password from './PasswordForm';

type AccountFormProps = {
  loading?: boolean;
  user?: UserProps;
  theme: Theme;
  createAccount?: any;
  updateAccount?: any;
  resetResponseUser?: any;
  response?: {
    message: string;
    success: boolean;
  };
} & StackScreenProps<RootStackParamList, 'Account' | 'Login'>;

const AccountForm: FC<AccountFormProps> = ({
  theme,
  createAccount,
  resetResponseUser,
  updateAccount,
  response,
  loading,
  navigation,
  user,
}) => {
  const styles = useStyles();

  const [editing, setEditing] = useState(false);

  const [form, setForm] = useState<UserProps & { confirmPassword: string }>({
    id: undefined,
    firstName: '',
    lastName: '',
    email: '',
    confirmPassword: '',
    password: '',
    phoneNumber: '',
    userName: '',
  });

  const [validation, setValidation] = useState({
    firstName: {
      valid: false,
      msg: '',
    },
    lastName: {
      valid: false,
      msg: '',
    },
    email: {
      valid: false,
      msg: '',
    },
    userName: {
      valid: false,
      msg: '',
    },
    phoneNumber: {
      valid: true,
      msg: '',
    },
    password: {
      valid: false,
      msg: '',
    },
    confirmPassword: {
      valid: false,
      msg: '',
    },
  });

  const [submit, setSubmit] = useState(false);
  const [error, setError] = useState<string | undefined>('');

  useEffect(() => {
    if (
      validation.firstName.valid &&
      validation.lastName.valid &&
      validation.email.valid &&
      validation.phoneNumber.valid &&
      validation.userName.valid &&
      (editing ||
        (validation.password.valid && validation.confirmPassword.valid))
    ) {
      setSubmit(true);
    } else {
      setSubmit(false);
    }
  }, [validation]);

  useEffect(() => {
    if (!loading && response?.success && !editing) {
      resetResponseUser();
      navigation.reset({ index: 0, routes: [{ name: 'Root' }] });
    } else if (!response?.success) {
      setError(response?.message);
    }
  }, [response, loading]);

  useEffect(() => {
    if (user?.id) {
      setForm({
        ...form,
        ...user,
        phoneNumber: user.phoneNumber ? maskPhone(user.phoneNumber) : '',
      });
      setValidation({
        ...validation,
        firstName: {
          valid: true,
          msg: '',
        },
        lastName: {
          valid: true,
          msg: '',
        },
        email: {
          valid: true,
          msg: '',
        },
        userName: {
          valid: true,
          msg: '',
        },
      });
      setEditing(true);
    }
  }, [user]);

  const onSubmit = () => {
    if (submit) {
      const normalizePhone = form.phoneNumber
        ? removeMaskToNumber(form.phoneNumber)
        : '';
      if (editing) {
        updateAccount({
          ...omit(['password', 'confirmPassword', 'userName', 'email'], form),
          phoneNumber: normalizePhone,
        });
      } else {
        createAccount({
          ...omit(['id'], form),
          phoneNumber: normalizePhone,
        });
      }
    }
  };

  return (
    <View>
      <Text style={styles.error}>{error}</Text>
      <View>
        <Input
          label="Nome"
          placeholder="Digite o nome ..."
          value={form.firstName}
          nameInput="firstName"
          validation={validation}
          disabled={loading}
          onSubmitEditing={onSubmit}
          leftIcon={
            <Icon
              size={23}
              name="card-account-details-star"
              color={theme.base}
            />
          }
          onChangeText={(text) => {
            const validate = haslengthTree(text);
            setForm({
              ...form,
              firstName: text,
            });
            setValidation({
              ...validation,
              firstName: {
                ...validate,
              },
            });
          }}
        />
        <Input
          label="Sobrenome"
          placeholder="Digite o sobrenome ..."
          value={form.lastName}
          nameInput="lastName"
          validation={validation}
          disabled={loading}
          onSubmitEditing={onSubmit}
          leftIcon={
            <Icon
              size={23}
              name="card-account-details-star"
              color={theme.base}
            />
          }
          onChangeText={(text) => {
            const validate = haslengthTree(text);
            setForm({
              ...form,
              lastName: text,
            });
            setValidation({
              ...validation,
              lastName: {
                ...validate,
              },
            });
          }}
        />
        <Input
          label="Telefone"
          placeholder="Digite o telefone ..."
          value={form.phoneNumber}
          nameInput="phoneNumber"
          validation={validation}
          maxLength={15}
          returnKeyType="done"
          keyboardType="phone-pad"
          disabled={loading}
          onSubmitEditing={onSubmit}
          leftIcon={
            <Icon size={23} name="card-account-phone" color={theme.base} />
          }
          onChangeText={(text) => {
            const validate = hasPhone(text);
            setForm({
              ...form,
              phoneNumber: maskPhone(text),
            });
            setValidation({
              ...validation,
              phoneNumber: {
                ...validate,
              },
            });
          }}
        />
        <Input
          label="Usuário"
          placeholder="Digite o nome do usuário ..."
          value={form.userName}
          nameInput="userName"
          validation={validation}
          disabled={loading || editing}
          onSubmitEditing={onSubmit}
          leftIcon={
            <Icon size={23} name="card-account-details" color={theme.base} />
          }
          onChangeText={(text) => {
            const validate = haslengthTree(text);
            setForm({
              ...form,
              userName: trim(text),
            });
            setValidation({
              ...validation,
              userName: {
                ...validate,
              },
            });
          }}
        />
        <Input
          label="Email"
          placeholder="Digite o email ..."
          value={form.email}
          nameInput="email"
          validation={validation}
          onSubmitEditing={onSubmit}
          disabled={loading || editing}
          keyboardType="email-address"
          leftIcon={
            <Icon size={23} name="card-account-mail" color={theme.base} />
          }
          onChangeText={(text) => {
            const validate = hasEmail(text);
            setForm({
              ...form,
              email: text,
            });
            setValidation({
              ...validation,
              email: {
                ...validate,
              },
            });
          }}
        />
        {!editing && (
          <Password
            loading={loading}
            theme={theme}
            form={form}
            validation={validation}
            setForm={setForm}
            setValidation={setValidation}
            onSubmit={onSubmit}
          />
        )}
      </View>
      <View style={styles.buttonContainer}>
        {editing && (
          <Button
            title="Trocar senha"
            style={{ marginBottom: 10 }}
            type="outline"
            disabled={loading}
            onPress={() => navigation.navigate('ResetPass')}
          />
        )}
        <Button
          title="Confirmar"
          type="outline"
          disabled={!submit || loading}
          onPress={onSubmit}
        />
      </View>
    </View>
  );
};

const useStyles = () => {
  return StyleSheet.create({
    buttonContainer: {
      alignItems: 'center',
      marginTop: 10,
      marginBottom: 10,
    },
    error: {
      textAlign: 'center',
      color: 'red',
      fontSize: 17,
      marginBottom: 2,
    },
  });
};

const mapToProps = (state: ReducersState) => {
  return {
    loading: state.user.loading,
    response: state.user.response,
  };
};

export default withRedux(mapToProps, {
  createAccount,
  resetResponseUser,
  updateAccount,
})(AccountForm);
