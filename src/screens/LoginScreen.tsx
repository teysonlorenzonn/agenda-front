import { StackScreenProps } from '@react-navigation/stack';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import React, { useState, FC, useEffect } from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View as ViewRN,
} from 'react-native';
import { View } from '../components/Themed';

import { RootStackParamList } from '../navigation/types';
import { Theme } from '../constants/Colors';

import { hasUserAndEmail } from '../utils/validations';
import { isEmpty } from 'ramda';
import KeyboardAvoiding from '../components/KeyboardAvoiding';
import Input from '../components/Input';
import Button from '../components/Button';
import useTheme from '../hooks/useTheme';
import { withRedux, ReducersState } from '../store/connectRedux';
import { logIn, resetResponseUser } from '../store/reducers/user/userReducer';
import LoaderPage from '../components/LoaderContent';
import Card from '../components/Card';

type LoaginProps = {
  loading: boolean;
  logIn: any;
  resetResponseUser: any;
  response: {
    message: string;
    success: boolean;
  };
} & StackScreenProps<RootStackParamList, 'Login'>;

const Login: FC<LoaginProps> = ({
  navigation,
  loading,
  response,
  logIn,
  resetResponseUser,
}) => {
  const theme = useTheme();
  const styles = useStyles(theme);
  const [submit, setSubmit] = useState(false);
  const [passLock, setPassLock] = useState(true);
  const [error, setError] = useState('');

  const [form, setForm] = useState({
    login: '',
    password: '',
  });

  const [validation, setValidation] = useState({
    login: {
      msg: '',
      valid: false,
    },
    password: {
      msg: '',
      valid: false,
    },
  });

  useEffect(() => {
    if (validation.password.valid && validation.login.valid) {
      setSubmit(true);
    } else {
      setSubmit(false);
    }
  }, [validation]);

  useEffect(() => {
    if (!loading && response.success) {
      resetResponseUser();
      navigation.replace('Root');
    } else if (!response.success) {
      setError(response.message);
    }
  }, [response, loading]);

  useEffect(() => {
    resetResponseUser();
  }, [form]);

  const onSubmit = () => {
    if (submit) {
      logIn(form);
    }
  };

  return (
    <KeyboardAvoiding>
      <View style={styles.container}>
        <ViewRN style={styles.titleContainer}>
          <Text style={styles.title}>Bem vindo</Text>
        </ViewRN>
        <LoaderPage active={loading} color={theme.base}>
          <Card containerStyle={styles.card}>
            <Text style={styles.error}>{error}</Text>
            <ViewRN>
              <Input
                label="Usuário"
                disabled={loading}
                validation={validation}
                nameInput="login"
                value={form.login}
                placeholder="Digite o nome do usuário ou email ..."
                leftIcon={
                  <MaterialCommunityIcons
                    size={23}
                    name="email"
                    color={theme.base}
                  />
                }
                onChangeText={(text) => {
                  setForm({ ...form, login: text });
                  const validate = hasUserAndEmail(text);
                  setValidation({
                    ...validation,
                    login: { valid: validate.valid, msg: validate.msg },
                  });
                }}
                onSubmitEditing={onSubmit}
              />
              <Input
                label="Senha"
                disabled={loading}
                validation={validation}
                nameInput="password"
                value={form.password}
                secureTextEntry={passLock}
                placeholder="Digite sua senha ..."
                leftIcon={
                  <MaterialCommunityIcons
                    size={23}
                    name="lock"
                    color={theme.base}
                  />
                }
                rightIcon={
                  <TouchableOpacity onPress={() => setPassLock(!passLock)}>
                    <MaterialCommunityIcons
                      size={23}
                      name={passLock ? 'eye-off' : 'eye'}
                      color={theme.base}
                    />
                  </TouchableOpacity>
                }
                onChangeText={(text) => {
                  setForm({ ...form, password: text });
                  const validate = isEmpty(text)
                    ? { valid: false, msg: '' }
                    : { valid: true, msg: '' };
                  setValidation({
                    ...validation,
                    password: { valid: validate.valid, msg: validate.msg },
                  });
                }}
                onSubmitEditing={onSubmit}
              />
            </ViewRN>

            <ViewRN style={styles.buttonContainer}>
              <Button
                title="Entrar"
                type="outline"
                onPress={onSubmit}
                disabled={loading || !submit}
              />
            </ViewRN>
            <ViewRN style={styles.buttonContainer}>
              <Button
                title="Criar conta"
                type="outline"
                disabled={loading}
                onPress={() => navigation.navigate('Account')}
              />
            </ViewRN>
          </Card>
        </LoaderPage>
      </View>
    </KeyboardAvoiding>
  );
};

const useStyles = (theme: Theme) => {
  return StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      fontSize: 38,
      position: 'absolute',
      bottom: 10,
      left: 15,
      fontWeight: 'bold',
      color: theme.base,
    },
    card: {
      width: '100%',
      borderWidth: 0,
      borderTopRightRadius: 15,
      borderTopLeftRadius: 15,
      padding: 15,
      height: '70%',
    },
    titleContainer: {
      height: '30%',
      width: '100%',
    },
    inputs: {
      color: theme.text,
    },
    buttonContainer: {
      alignItems: 'center',
      marginTop: 10,
    },
    createAccount: {
      alignItems: 'flex-end',
      marginBottom: 10,
      paddingRight: 12,
    },
    createAccountText: {
      color: theme.tint,
      fontSize: 16,
    },
    error: {
      textAlign: 'center',
      color: 'red',
      fontSize: 17,
      marginBottom: 2,
    },
  });
};

const mapToProps = (state: ReducersState) => {
  return {
    loading: state.user.loading,
    response: state.user.response,
  };
};

export default withRedux(mapToProps, { logIn, resetResponseUser })(Login);
