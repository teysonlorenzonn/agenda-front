import React, { FC, useState, useEffect, useMemo, useRef } from 'react';
import { MaterialCommunityIcons as Icon } from '@expo/vector-icons';
import {
  StyleSheet,
  View as ViewRN,
  TextInput,
  TouchableOpacity,
  Text,
} from 'react-native';

import { Theme } from '../../constants/Colors';
import { View } from '../../components/Themed';
import useTheme from '../../hooks/useTheme';
import SpeedDial from '../../components/SpeedDial';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from '../../navigation/types';
import Card from '../../components/Card';
import { isEmpty, union } from 'ramda';
import { ReducersState, withRedux } from '../../store/connectRedux';
import {
  createList,
  fetchLists,
  deleteList,
  updateList,
} from '../../store/reducers/lists/listsReducer';
import { ListsProps } from '../../store/reducers/lists/types';
import { KeyboardAwareFlatList } from 'react-native-keyboard-aware-scroll-view';
import ModalView from '../../components/Modal';
import Button from '../../components/Button';
import { filterLike } from '../../utils/funcs';
import Search from '../../components/Search';

type Props = {
  lists: Array<Props>;
  listsApi: Array<ListsProps>;
  loading: boolean;
  createList: any;
  fetchLists: any;
  deleteList: any;
  updateList: any;
} & StackScreenProps<RootStackParamList, 'Login'>;

export const Lists: FC<Props> = ({
  fetchLists,
  listsApi,
  loading,
  ...props
}) => {
  const [lists, setLists] = useState<Array<ListsProps>>([]);
  const [search, setSearch] = useState('');
  const [openSearch, setOpenSearch] = useState(false);

  const theme = useTheme();
  const styles = useStyles(theme);

  useEffect(() => {
    fetchLists();
  }, []);

  useEffect(() => {
    if (listsApi.length > 0) {
      setLists(listsApi);
    }
  }, [listsApi]);

  const exibition = useMemo(() => {
    if (search.length > 0) {
      return filterLike(search, 'name', lists);
    }
    return lists;
  }, [lists, search]);

  return (
    <View style={styles.container}>
      <Search
        search={search}
        setSearch={setSearch}
        openSearch={openSearch}
        setOpenSearch={setOpenSearch}
      >
        {({ fadeIn }: { fadeIn: () => void }) => (
          <>
            <KeyboardAwareFlatList
              data={exibition}
              refreshing={loading}
              onRefresh={fetchLists}
              renderItem={({ item, index }) => (
                <ListItems
                  key={index + item.name}
                  item={item}
                  theme={theme}
                  index={index}
                  {...props}
                />
              )}
            />
            <SpeedDial
              add
              search
              functionAdd={() => {
                setLists(
                  union([{ name: '', description: '', items: [] }], lists),
                );
              }}
              functionSearch={fadeIn}
              {...props}
            />
          </>
        )}
      </Search>
    </View>
  );
};

type ListItemsProps = {
  item: ListsProps;
  theme: Theme;
  index: number;
  createList: any;
  deleteList: any;
  updateList: any;
} & StackScreenProps<RootStackParamList, 'Login'>;

export const ListItems: FC<ListItemsProps> = ({
  item,
  theme,
  index,
  createList,
  deleteList,
  updateList,
  navigation,
}) => {
  const [editing, setEditing] = useState(false);
  const [autoFocus, setAutoFocus] = useState(false);
  const [blurActive, setBlurActive] = useState({ input: false, input2: false });
  const [form, setForm] = useState<ListsProps>(item);
  const [open, setOpen] = useState(false);

  const titleRef = useRef<TextInput>(null);
  const styles = useStyles(theme);

  const validSub = useMemo(
    () => !isEmpty(form.name) && !isEmpty(form.description),
    [form],
  );

  useEffect(() => {
    if (index === 0 && !validSub) {
      setEditing(true);
      setAutoFocus(true);
    }
  }, [index, form]);

  const onSubmit = () => {
    if (!editing) {
      titleRef.current?.focus();
    }
    if (editing && validSub) {
      if (form.id) {
        updateList(form);
      } else {
        createList(form);
      }
    }
    setBlurActive({ input: false, input2: false });
    setEditing(!editing);
  };

  const onBlurSubmit = () =>
    blurActive.input && blurActive.input2 && onSubmit();

  const onEnter = () => {
    navigation.navigate('ItemList', { idList: form.id });
  };

  return (
    <TouchableOpacity
      key={index}
      activeOpacity={0.9}
      disabled={editing}
      onPress={onEnter}
    >
      <ModalView
        open={open}
        setOpen={setOpen}
        render={
          <DeleteView
            deleteList={deleteList}
            id={form.id}
            styles={styles}
            setOpen={setOpen}
          />
        }
      >
        <Card containerStyle={{ marginBottom: 0, marginTop: 5 }}>
          <ViewRN style={styles.header}>
            <TouchableOpacity
              style={{ paddingTop: 5, paddingLeft: 5, paddingRight: 5 }}
              onPress={onSubmit}
            >
              <Icon
                name={editing ? 'content-save' : 'pencil'}
                size={25}
                color={editing ? theme.base : theme.tint}
              />
            </TouchableOpacity>
            {!editing && (
              <TouchableOpacity
                style={{ paddingTop: 5, paddingLeft: 5, paddingRight: 5 }}
                onPress={() => form.id && setOpen(true)}
              >
                <Icon
                  name="delete-forever"
                  size={25}
                  color={open ? theme.base : theme.tint}
                />
              </TouchableOpacity>
            )}
          </ViewRN>
          <ViewRN>
            {editing ? (
              <>
                <TextInput
                  style={styles.title}
                  placeholderTextColor={theme.disabled}
                  multiline
                  numberOfLines={2}
                  maxLength={40}
                  placeholder="Digite o titulo da lista ..."
                  value={form.name}
                  autoFocus={autoFocus}
                  editable={editing}
                  onBlur={onBlurSubmit}
                  onChangeText={(text) => {
                    setForm({ ...form, name: text });
                    setBlurActive({ ...blurActive, input: true });
                  }}
                />
                <TextInput
                  onTouchStart={() => !editing && onEnter()}
                  style={styles.description}
                  editable={editing}
                  placeholderTextColor={theme.disabled}
                  multiline
                  placeholder="Digite a descrição da lista ..."
                  value={form.description}
                  onBlur={onBlurSubmit}
                  onChangeText={(text) => {
                    setForm({ ...form, description: text });
                    setBlurActive({ ...blurActive, input2: true });
                  }}
                />
              </>
            ) : (
              <>
                <Text style={styles.title}>{form.name}</Text>
                <Text style={styles.description}>{form.description}</Text>
              </>
            )}
          </ViewRN>
        </Card>
      </ModalView>
    </TouchableOpacity>
  );
};

type DeleteProps = {
  deleteList: any;
  id?: string;
  styles: any;
  setOpen: any;
};

const DeleteView: FC<DeleteProps> = ({ deleteList, id, styles, setOpen }) => {
  return (
    <ViewRN>
      <Text style={styles.deleteText}>Deseja realmente deletar a lista ?</Text>
      <ViewRN style={styles.buttonView}>
        <Button
          title="Sim"
          type="outline"
          width={70}
          onPress={() => deleteList(id)}
        />
        <Button
          style={{ marginLeft: 7 }}
          title="Não"
          type="outline"
          width={70}
          onPress={() => setOpen(false)}
        />
      </ViewRN>
    </ViewRN>
  );
};

const useStyles = (theme: Theme) =>
  StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: 20,
    },
    title: {
      fontSize: 28,
      color: theme.base,
      padding: 5,
      margin: 0,
    },
    description: {
      fontSize: 17,
      color: theme.text,
      padding: 5,
      margin: 0,
    },
    header: {
      flexDirection: 'row',
      justifyContent: 'flex-end',
    },
    deleteText: {
      color: theme.text,
      fontSize: 25,
    },
    buttonView: {
      flexDirection: 'row',
      justifyContent: 'center',
      marginTop: 20,
    },
  });

const mapToProps = (state: ReducersState) => {
  return {
    loading: state.lists.loading,
    listsApi: state.lists.lists,
    response: state.lists.response,
  };
};

export default withRedux(mapToProps, {
  createList,
  fetchLists,
  deleteList,
  updateList,
})(Lists);
