import { StackScreenProps } from '@react-navigation/stack';
import React, { FC } from 'react';
import { Text } from 'react-native';
import Header from '../../components/Header';
import { View } from '../../components/Themed';
import { RootStackParamList } from '../../navigation/types';

const ItemList: FC<StackScreenProps<RootStackParamList, 'ItemList'>> = ({
  route,
  navigation,
}) => {
  const { idList } = route.params;

  return (
    <View>
      <Header route={route} navigation={navigation} title="Itens" />
      <Text style={{ color: 'white' }}>To ai {idList}</Text>
    </View>
  );
};

export default ItemList;
